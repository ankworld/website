import { DOCUMENT } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { map, share, Subscription, timer } from 'rxjs';

import { User, UserService } from '../../core';

@Component({
  selector: 'app-layout-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  clock = new Date();
  subscription: Subscription | undefined;

  currentUser: User | undefined;

  elem!: HTMLElement & {
    mozRequestFullScreen(): Promise<void>;
    webkitRequestFullscreen(): Promise<void>;
    msRequestFullscreen(): Promise<void>;
  };

  fullscreenEnabled = false;
  isFullscreen = true;

  constructor(
    private userService: UserService,
    @Inject(DOCUMENT) private document: any
  ) {}

  ngOnInit() {
    this.userService.currentUser.subscribe((userData) => {
      this.currentUser = userData;
    });

    this.subscription = timer(0, 1000)
      .pipe(
        map(() => new Date()),
        share()
      )
      .subscribe((time) => {
        this.clock = time;
      });

    this.elem = this.document.documentElement as HTMLElement & {
      mozRequestFullScreen(): Promise<void>;
      webkitRequestFullscreen(): Promise<void>;
      msRequestFullscreen(): Promise<void>;
    };
    this.fullscreenEnabled = this.document.fullscreenEnabled;
    this.isFullscreen = this.isInFullscreen();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  // REF: https://imvikaskohli.medium.com/how-to-implement-fullscreen-mode-in-angular-fb9f55c24f67
  async openFullscreen() {
    if (this.elem.requestFullscreen) {
      await this.elem.requestFullscreen();
    } else if (this.elem.mozRequestFullScreen) {
      /* Firefox */
      await this.elem.mozRequestFullScreen();
    } else if (this.elem.webkitRequestFullscreen) {
      /* Chrome, Safari and Opera */
      await this.elem.webkitRequestFullscreen();
    } else if (this.elem.msRequestFullscreen) {
      /* IE/Edge */
      await this.elem.msRequestFullscreen();
    }
    this.isInFullscreen();
  }

  // REF: https://imvikaskohli.medium.com/how-to-implement-fullscreen-mode-in-angular-fb9f55c24f67
  async closeFullscreen() {
    if (this.document.exitFullscreen) {
      await this.document.exitFullscreen();
    } else if (this.document.mozCancelFullScreen) {
      /* Firefox */
      await this.document.mozCancelFullScreen();
    } else if (this.document.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      await this.document.webkitExitFullscreen();
    } else if (this.document.msExitFullscreen) {
      /* IE/Edge */
      await this.document.msExitFullscreen();
    }
    this.isInFullscreen();
  }

  isInFullscreen() {
    if (this.document.fullscreenElement) {
      this.isFullscreen = true;
      return true;
    }
    this.isFullscreen = false;
    return false;
  }
}
