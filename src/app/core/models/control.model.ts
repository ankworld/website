export interface IControl {
  width?: number;
  height?: number;
  index?: number;
  component?: any;
}

export class Control implements IControl {
  constructor(
    public width?: number,
    public height?: number,
    public index?: number,
    public component?: any
  ) {}
}
