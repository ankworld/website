import { Type } from '@angular/core';

export class AppItem {
  constructor(public component: Type<any>, public data: any) {}
}
