import {
  Component,
  OnInit,
  VERSION,
  ViewChildren,
  ElementRef,
  QueryList,
  NgZone,
  AfterViewInit
} from '@angular/core';
import {
  trigger,
  style,
  animate,
  transition,
  state
} from '@angular/animations';
import { Control, IControl } from '../core';
import { CdkDragMove } from '@angular/cdk/drag-drop';
import { ContactComponent } from '../contact/contact.component';

@Component({
  selector: 'app-home',
  animations: [
    trigger('fadeInOut', [
      state(
        'void',
        style({
          opacity: 0
        })
      ),
      transition('void <=> *', animate(200))
    ]),
    trigger('scaleInOut', [
      state('in', style({ transform: 'scale(1)', opacity: 1 })),
      transition('void => *', [
        style({ transform: 'scale(0.8)', opacity: 0 }),
        animate(300)
      ]),
      transition('* => void', [
        animate(1000, style({ transform: 'scale(3)', opacity: 0 }))
      ])
    ])
  ],
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {
  selectedControl?: Control;
  controls?: Control[];
  lockAxis?: any = 'x|y';
  @ViewChildren('resizeBox') resizeBox?: QueryList<ElementRef>;
  @ViewChildren('dragHandleRB') dragHandleRB?: QueryList<ElementRef>;
  @ViewChildren('dragHandleRight') dragHandleRight?: QueryList<ElementRef>;
  @ViewChildren('dragHandleBottom') dragHandleBottom?: QueryList<ElementRef>;

  globalZIndex = 0;

  constructor(private ngZone: NgZone) {
    this.controls = [];
  }

  ngOnInit(): void {}

  ngAfterViewInit(): void {
    this.addControl();
  }

  addControl(): void {
    const templateControl = new Control();
    templateControl.width = 400;
    templateControl.height = 400;
    templateControl.index =
      this.controls === undefined ? 0 : this.controls.length;

    templateControl.component = ContactComponent;

    this.controls?.push(templateControl);
    this.selectedControl = templateControl;

    this.setCreateHandleTransform();
  }

  setCreateHandleTransform(): void {
    let rect: any = null;
    this.resizeBox!.changes.subscribe(() => {
      rect = this.resizeBox!.filter(
        (element, index) => index === this.selectedControl!.index!
      )[0].nativeElement.getBoundingClientRect();
      this.dragHandleRB!.changes.subscribe(() => {
        this.setHandleTransform(
          this.dragHandleRB!.filter(
            (element, index) => index === this.selectedControl!.index!
          )[0].nativeElement,
          rect,
          'both'
        );
      });

      this.dragHandleBottom!.changes.subscribe(() => {
        this.setHandleTransform(
          this.dragHandleBottom!.filter(
            (element, index) => index === this.selectedControl!.index!
          )[0].nativeElement,
          rect,
          'y'
        );
      });

      this.dragHandleRight!.changes.subscribe(() => {
        this.setHandleTransform(
          this.dragHandleRight!.filter(
            (element, index) => index === this.selectedControl!.index!
          )[0].nativeElement,
          rect,
          'x'
        );
      });
    });
  }

  setUpdateHandleTransform(): void {
    const rect = this.resizeBox!.filter(
      (element, index) => index === this.selectedControl!.index!
    )[0].nativeElement.getBoundingClientRect();
    this.setHandleTransform(
      this.dragHandleBottom!.filter(
        (element, index) => index === this.selectedControl!.index!
      )[0].nativeElement,
      rect,
      'y'
    );
    this.setHandleTransform(
      this.dragHandleRB!.filter(
        (element, index) => index === this.selectedControl!.index!
      )[0].nativeElement,
      rect,
      'both'
    );
    this.setHandleTransform(
      this.dragHandleRight!.filter(
        (element, index) => index === this.selectedControl!.index!
      )[0].nativeElement,
      rect,
      'x'
    );
  }

  setHandleTransform(
    dragHandle: HTMLElement,
    targetRect: DOMRect,
    position: 'x' | 'y' | 'both'
  ): void {
    const dragRect = dragHandle.getBoundingClientRect();
    const translateX = targetRect.width - dragRect.width;
    const translateY = targetRect.height - dragRect.height;
    // eslint-disable-next-line no-console
    // console.log(translateX + ':' + translateY);
    if (position === 'x') {
      dragHandle.style.transform = `translate3d(${translateX}px, 0, 0)`;
    }

    if (position === 'y') {
      dragHandle.style.transform = `translate3d(0, ${translateY}px, 0)`;
    }

    if (position === 'both') {
      dragHandle.style.transform = `translate3d(${translateX}px, ${translateY}px, 0)`;
    }
  }

  dragMove(
    dragHandle: HTMLElement,
    $event: CdkDragMove<any>,
    control: Control
  ): void {
    this.selectedControl = control;
    this.ngZone.runOutsideAngular(() => {
      this.resize(
        dragHandle,
        this.resizeBox!.filter((element, index) => index === control.index!)[0]
          .nativeElement
      );
    });
  }

  resize(dragHandle: HTMLElement, target: HTMLElement): void {
    const dragRect = dragHandle.getBoundingClientRect();
    const targetRect = target.getBoundingClientRect();

    const width = dragRect.left - targetRect.left + dragRect.width;
    const height = dragRect.top - targetRect.top + dragRect.height;

    target.style.width = width + 'px';
    target.style.height = height + 'px';

    this.setUpdateHandleTransform();
  }

  clickControl(control: Control): void {
    this.selectedControl = control;
    const target = this.resizeBox!.filter(
      (element, index) => index === this.selectedControl!.index!
    )[0].nativeElement;
    target.style.zIndex = this.globalZIndex + 1;
    this.globalZIndex += 1;
  }

  // TODO: Better remove control handle
  removeControl(control: Control) {
    if (control.index !== undefined) {
      const target = this.resizeBox!.filter(
        (element, index) => index === control!.index!
      )[0].nativeElement;
      target.remove();
    }
  }
}
