import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';

import { DragDropModule } from '@angular/cdk/drag-drop';
import { MatIconModule } from '@angular/material/icon';
import { CoreModule } from '../core';
import { ContactModule } from '../contact/contact.module';
import { MatButtonModule } from '@angular/material/button';
import { MatTooltipModule } from '@angular/material/tooltip';

import { NgScrollbarModule } from 'ngx-scrollbar';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    HomeRoutingModule,
    DragDropModule,
    MatIconModule,
    MatButtonModule,
    MatTooltipModule,
    NgScrollbarModule,
    CoreModule,
    ContactModule
  ]
})
export class HomeModule {}
